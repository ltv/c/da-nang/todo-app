const express = require('express');
const allRouter = require('express-list-endpoints');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const router = require('./routes/todo.Router');
const port = 5555;
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', router);
app.listen(port, () => {
  console.log(`server run port ${port}`);
  console.log(allRouter(app));
});
module.exports = app;
