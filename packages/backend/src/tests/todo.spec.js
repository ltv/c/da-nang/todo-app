const todos = require('../models/todo.Model');
describe('Test todo model', () => {
  it('Should create todo & return todo', () => {
    const inserted = todos.insert({ id: 1, title: 'home work' });
    expect.assertions(3);
    expect(inserted).toEqual({ id: 1, title: 'home work', completed: false });
    expect(todos.todos.length).toEqual(1);
    expect(todos.todos[0]).toEqual({
      id: 1,
      title: 'home work',
      completed: false,
    });
  });

  it('Should update todo & return todo', () => {
    const updated = todos.updateById({
      id: 1,
      title: 'done home work',
    });
    expect.assertions(4);
    expect(updated.id).toEqual(1);
    expect(updated.title).toEqual('done home work');
    expect(updated.completed).toEqual(false);
    expect(todos.todos[0].title).toEqual('done home work');
  });

  it('Should update todo not found id & return false', () => {
    const updated = todos.updateById({ id: 2, title: 'done home work' });
    expect(updated).toEqual(false);
  });

  it('Should delete todo & return true', () => {
    const deleted = todos.deleteById(1);
    expect(deleted).toEqual(true);
  });

  it('Should delete todo not found id & return false', () => {
    const deleted = todos.deleteById(2);
    expect(deleted).toEqual(false);
  });

  it('Should find all todos not params & return all todos', () => {
    const allTodos = todos.findAll();
    expect(allTodos).toEqual(todos.todos);
  });

  it('Should find all todos & return todos', () => {
    todos.insert({ id: 1, title: 'home work' });
    todos.insert({ id: 2, title: 'home work' });
    todos.insert({ id: 3, title: 'home work' });
    todos.updateById({ id: 1, completed: true });
    const allTodos = todos.findAll({ completed: true });
    expect(allTodos.length).toEqual(1);
  });
});
