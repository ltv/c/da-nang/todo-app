const todoController = require('../controllers/todo.Controller');
const { mockRequest, mockResponse } = require('./interceptor');
const app = require('../app');
const request = require('supertest');
describe('Test todo Controllers', () => {
  it('Create todo & return new todo', async () => {
    const res = await request(app)
      .post('/api/createTodo')
      .send({ id: 1, title: 'First Todo' });
    expect.assertions(2);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({ id: 1, title: 'First Todo', completed: false });
  });
});
describe('Test todo controller', () => {
  it('Create toto ', async () => {
    let req = mockRequest();
    req.body.id = 1;
    req.body.title = 'first';
    const res = mockResponse();
    await todoController.createTodo(req, res);
    expect.assertions(4);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json.mock.calls.length).toBe(1);
    expect(res.json.mock.calls[0][0].id).toEqual(1);
    expect(res.json.mock.calls[0][0].title).toEqual('first');
  });

  it('Create toto id && title empty  ', async () => {
    let req = mockRequest();
    const res = mockResponse();
    await todoController.createTodo(req, res);
    expect.assertions(3);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json.mock.calls.length).toBe(1);
    expect(res.json.mock.calls[0][0].mess).toEqual('id or title are not empty');
  });

  it('Upadate todo by id', async () => {
    let req = mockRequest();
    req.body.id = 1;
    req.body.title = 'first-1';
    req.body.compeleted = true;
    const res = mockResponse();
    await todoController.updateTodo(req, res);
    expect.assertions(2);
    expect(res.json.mock.calls[0][0].title).toEqual('first-1');
    expect(res.json.mock.calls[0][0].compeleted).toEqual(true);
  });

  it('Upadate todo by id diffrent', async () => {
    let req = mockRequest();
    req.body.id = 2;
    const res = mockResponse();
    await todoController.updateTodo(req, res);
    expect.assertions(1);
    expect(res.json.mock.calls[0][0]).toEqual(false);
  });

  it('Upadate todo by id diffrent', async () => {
    let req = mockRequest();
    const res = mockResponse();
    await todoController.updateTodo(req, res);
    expect.assertions(1);
    expect(res.json.mock.calls[0][0].mess).toEqual('id is not empty');
  });

  it('Get all todos', async () => {
    let req = mockRequest();
    const res = mockResponse();
    await todoController.todos(req, res);
    expect.assertions(1);
    expect(res.json.mock.calls.length).toEqual(1);
  });
  // need fix
  // it('Get all todos completed : true', async () => {
  //   let req = mockRequest();
  //   req.params.compeleted = 'true';
  //   const posts = [{ id: 1, title: 'first-1', completed: true }];
  //   const res = mockResponse();
  //   await todoController.todosCompleted(req, res);
  //   expect.assertions(1);
  //   expect(res.json).toBeCalledWith(posts);
  // });
  // need fix
  it('Get all todos completed : false', async () => {
    const posts = [];
    let req = mockRequest();
    req.params.compeleted = false;
    const res = mockResponse();
    await todoController.todosCompleted(req, res);
    // expect.as)sertions(1);
    expect(res.json).toBeCalledWith(posts);
  });

  it('Delete Todo by id return todo deleted', async () => {
    let req = mockRequest();
    req.body.id = 1;
    const res = mockResponse();
    await todoController.deleteTodo(req, res);
    expect.assertions(1);
    expect(res.json).toBeCalledWith(true);
  });

  it('Delete Todo id empty', async () => {
    let req = mockRequest();
    const res = mockResponse();
    await todoController.deleteTodo(req, res);
    expect.assertions(1);
    expect(res.json.mock.calls[0][0].mess).toEqual('id is not empty');
  });

  it('Delete complete return true', async () => {
    let req = mockRequest();
    const res = mockResponse();
    await todoController.clearCompleted(req, res);
    expect.assertions(1);
    expect(res.json).toBeCalledWith(true);
  });
});
