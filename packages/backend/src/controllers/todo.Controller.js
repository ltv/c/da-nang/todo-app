const Todos = require('../models/todo.Model');
module.exports.createTodo = (req, res) => {
  if (req.body.id != null && req.body.title != null) {
    const todo = Todos.insert(req.body);
    res.json(todo);
  } else {
    res.json({ mess: 'id or title are not empty' });
  }
};

module.exports.updateTodo = (req, res) => {
  const id = req.body.id;
  if (id) {
    const todo = Todos.updateById(req.body);
    res.json(todo);
  } else {
    res.json({ mess: 'id is not empty' });
  }
};

module.exports.todos = (req, res) => {
  const todos = Todos.findAll();
  res.json(todos);
};

module.exports.todosCompleted = (req, res) => {
  const { completed } = req.params;
  if (completed == 'true') {
    const todosCompleted = Todos.findAll({ completed: true });
    res.json(todosCompleted);
  } else if (completed == 'false') {
    const todosCompleted = Todos.findAll({ completed: false });
    res.json(todosCompleted);
  } else res.json([]);
};

module.exports.deleteTodo = (req, res) => {
  const id = req.body.id;
  if (id) {
    const deleted = Todos.deleteById(id);
    res.json(deleted);
  } else {
    res.json({ mess: 'id is not empty' });
  }
};
module.exports.clearCompleted = (req, res) => {
  const todos = Todos.findAll({ completed: true });
  for (let i = 0; i < todos.length; i++) {
    Todos.deleteById(todos[i].id);
  }
  res.json(true);
};
