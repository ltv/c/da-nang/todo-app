// Create Todo (Create single todo for each time) (/api/createTodo)
// Update Todo & Mark this todo as completed (/api/updateTodo)
// Get All Todo (/api/todos)
// Get All completed todos (/api/todos with params: completed: boolean)
// Get All activated todos (/api/todos with params: completed: false)
// Delete Todo (/api/deleteTodo)
// Clear Completed todos (/api/clearCompleted)

const express = require('express');
const router = express.Router();
const {
  createTodo,
  updateTodo,
  todos,
  deleteTodo,
  clearCompleted,
  todosCompleted,
} = require('../controllers/todo.Controller');
router.post('/createTodo', createTodo);
router.post('/updateTodo', updateTodo);
router.post('/todos', todos);
router.post('/todos/:completed', todosCompleted);
router.post('/deleteTodo', deleteTodo);
router.post('/clearCompleted', clearCompleted);
module.exports = router;
