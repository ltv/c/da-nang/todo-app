let todos = [];
/**
 *Insert todo to database
 *@todo{id,title,completed}
 */
exports.insert = todo => {
  const tobeTodo = { ...todo, completed: false };
  todos.push(tobeTodo);
  return tobeTodo;
};

/**
 * update todo by id
 * @param {id:number}
 * @return {todo}
 */
exports.updateById = todo => {
  let todoIndex = todos.findIndex(item => item.id === todo.id);
  if (todoIndex != -1) {
    todos[todoIndex] = { ...todos[todoIndex], ...todo };
    return todos[todoIndex];
  } else return false;
};

/**
 * delete todo by id
 *@param {id : number}
 *@return {boolean}
 */
exports.deleteById = id => {
  const todoIndex = todos.findIndex(item => item.id === id);
  if (todoIndex === -1) {
    return false;
  } else {
    todos.splice(todoIndex, 1);
    return true;
  }
};

/**
 *find all todos
 *@params {completed}
 *@return {list}
 */
exports.findAll = params => {
  if (!params) {
    return todos;
  }
  const { completed } = params;
  return todos.filter(item => item.completed === completed);
};

exports.todos = todos;
